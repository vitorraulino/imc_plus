package com.ifsc.francielle.imc_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class IMC_Plus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc__plus);
    }
    public void fazCalculo (View v){
        EditText peso_et = (EditText)findViewById(R.id.peso);
        EditText altura_et = (EditText)findViewById(R.id.altura);
        TextView resultado_tv = (TextView)findViewById(R.id.resultado);



        double peso = 0;
        double altura = 0;
        double resultado = 0;

        if(peso_et.getText().toString().equals("")){
            peso = 0;
        } else if(altura_et.getText().toString().equals("")){
            altura = 0;
        } else {
            peso = Double.parseDouble(peso_et.getText().toString());
            altura =  Double.parseDouble(altura_et.getText().toString());
        }

        if(peso > 0 && altura > 0){
            resultado = peso/(altura * altura);
            String rs = "" + resultado;
            resultado_tv.setText(rs.substring(0,5));
        } else {
            resultado_tv.setText("Nenhum dos valores pode ser 0 (Zero)!");
        }

    }
}
